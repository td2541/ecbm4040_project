
# coding: utf-8

# In[ ]:


import tensorflow as tf
import numpy as np
import collections

class Stylizer:
    
    def __init__(self, content_image, style_image, init_image, ids_content, ids_style, session, network, iteration, loss_ratio, norm, optm="L-BFGS"):
        
        self.CNN = network
        self.sess = session
        
        self.content_layers = collections.OrderedDict(sorted(ids_content.items()))
        self.style_layers = collections.OrderedDict(sorted(ids_style.items()))
        
        self.content_img = np.float32(self.CNN.normalization(content_image))
        self.style_img = np.float32(self.CNN.normalization(style_image))
        self.init_img = np.float32(self.CNN.normalization(init_image))
        
        self.loss_norm = norm
        self.iteration = iteration
        self.loss_ratio = loss_ratio
        self.optm = optm
        self.building()
        
    def building(self):
        
        #variable setting
        self.init = tf.Variable(self.init_img, trainable=True, dtype=tf.float32)
        self.content = tf.placeholder(tf.float32, shape=self.content_img.shape, name="content")
        self.style = tf.placeholder(tf.float32, shape=self.style_img.shape, name="style")
        
        #register layer feature
        content_layers = self.CNN.feed_forward(self.content, scope="content")
        style_layers = self.CNN.feed_forward(self.style, scope="style")
        self.content_p = {}
        self.style_a = {}
        for i in self.content_layers:
            self.content_p[i] = content_layers[i]
        for i in self.style_layers:
            self.style_a[i] = self.gram_mat(style_layers[i])
        self.stylizer_layer = self.CNN.feed_forward(self.init, scope="stylizer")
        
        #loss
        loss_content = 0
        loss_style = 0
        for i in self.stylizer_layer:
            if i in self.content_layers:
                loss_content = loss_content + self.LossFunc_content(i)
            if i in self.style_layers:
                loss_style = loss_style + self.LossFunc_style(i)
                
        alpha = 1
        beta = 1 / self.loss_ratio
        
        self.loss_content = loss_content
        self.loss_style = loss_style
        self.loss_total = alpha * loss_content + beta * loss_content
        
    def update(self):
        global loss_hist
        global itera
        itera = 0
        loss_hist = []
        def callback(t,c,s):
            global itera
            global loss_hist
            loss_hist.append(t)
            if itera % 100 == 0:
                print("For iteration: "+ str(itera)+ ", total loss: "+str(t)+", content loss: "+str(c)+", style loss: "+str(s))
            itera = itera + 1
            
        #optimizer setting
        print(self.iteration)
        if self.optm == "L-BFGS":
            optimizer = tf.contrib.opt.ScipyOptimizerInterface(self.loss_total, method = "L-BFGS-B",options={'maxiter': self.iteration})
        if self.optm == "Adam":
            optimizer = tf.train.AdamOptimizer(learning_rate=1e-3, beta1=0.9,beta2=0.999,epsilon=1e-08,use_locking=False,name='Adam')
        if self.optm == "SGD":
            optimizer = tf.train.GradientDescentOptimizer(learning_rate=1e-3,use_locking=False,name='GradientDescent')
            
        initalizer = tf.global_variables_initializer()
        self.sess.run(initalizer)
        
        optimizer.minimize(self.sess, feed_dict={self.content:self.content_img, self.style:self.style_img}, fetches=[self.loss_total, self.loss_content, self.loss_style], loss_callback=callback)
        
        result = self.sess.run(self.init)
        output_img = np.clip(self.CNN.denormalization(result), 0.0, 255.0)
        return output_img, loss_hist
        
    def LossFunc_content(self, index):
        fea_gen = self.stylizer_layer[index]
        fea_orig = self.content_p[index]
        
        batch, height, width, channel = fea_gen.shape
        N_l = height.value * width.value
        M_l = channel.value
        
        w = self.content_layers[index]
        
        if self.loss_norm == 1:
            loss = w * tf.reduce_sum(tf.pow((fea_gen-fea_orig), 2)) / 2
        elif self.loss_norm == 2:
            loss = w * tf.reduce_sum(tf.pow((fea_gen-fea_orig), 2)) / (N_l*M_l)
        elif self.loss_norm == 3:
            loss = w * (1. / (2. * np.sqrt(M_l) * np.sqrt(N_l))) * tf.reduce_sum(tf.pow((fea_gen - fea_orig), 2))
        return loss
    
    def LossFunc_style(self, index):
        fea_gen = self.stylizer_layer[index]
        fea_orig = self.style_a[index]
        batch, height, width, channel = fea_gen.shape
        N_l = height.value * width.value
        M_l = channel.value
        
        w = self.style_layers[index]
        
        gram = self.gram_mat(fea_gen)
        
        loss = w * (1. / (4 * (N_l**2) * (M_l**2))) * tf.reduce_sum(tf.pow((gram-fea_orig), 2))
        
        return loss
    
    def gram_mat(self, feature):
        num_channels = int(feature.shape[3])
        
        mat = tf.reshape(feature, shape=[-1, num_channels])
        gram = tf.matmul(tf.transpose(mat), mat)
   
        return gram
        

