
# coding: utf-8

# In[ ]:


import numpy as np
import tensorflow as tf
from scipy import io

#Building VGG19 Neural Network
VGG19_layers_names = (
    'conv1_1', 'relu1_1', 'conv1_2', 'relu1_2', 'pool1',

    'conv2_1', 'relu2_1', 'conv2_2', 'relu2_2', 'pool2',

    'conv3_1', 'relu3_1', 'conv3_2', 'relu3_2', 'conv3_3',
    'relu3_3', 'conv3_4', 'relu3_4', 'pool3',

    'conv4_1', 'relu4_1', 'conv4_2', 'relu4_2', 'conv4_3',
    'relu4_3', 'conv4_4', 'relu4_4', 'pool4',

    'conv5_1', 'relu5_1', 'conv5_2', 'relu5_2', 'conv5_3',
    'relu5_3', 'conv5_4', 'relu5_4'
)

VGG_mean= np.array([123.68, 116.779, 103.939])

class VGG19:
    
    def __init__(self, path, pool_type="avg"):
        data = io.loadmat(path)
        self.pool_type = pool_type
        self.weights = data["layers"][0]
        self.layers = VGG19_layers_names
        
    def conv_layer(self, tensor, weight, bias):
        w = tf.constant(weight)
        conv = tf.nn.conv2d(tensor, w, strides=(1,1,1,1), padding="SAME")
        result = tf.nn.bias_add(conv, bias)
        return result
    
    def pool_layer(self, tensor, pool_type="avg"):
        if pool_type == "avg":
            result = tf.nn.avg_pool(tensor, ksize=(1,2,2,1), strides=(1,2,2,1), padding="SAME")
        elif pool_type == "max":
            result = tf.nn.max_pool(tensor, ksize=(1,2,2,1), strides=(1,2,2,1), padding="SAME")
        else:
            raise ValueError("pool type should be avg or max!")
            
        return result
        
    def normalization(self, matrix):
        mat = matrix - VGG_mean
        return mat
    
    def denormalization(self, matrix):
        mat = matrix + VGG_mean
        return mat
    
    def feed_forward(self, image, scope=None):
        CNN = {}
        X = image
        p_type = self.pool_type
        
        with tf.variable_scope(scope):
            for ind, layer_name in enumerate(self.layers):
                layer_type = layer_name[:4]
                if layer_type == "conv":
                    
                    kernels = self.weights[ind][0][0][2][0][0]
                    bias = self.weights[ind][0][0][2][0][1]
                    
                    kernels = np.transpose(kernels, (1,0,2,3))
                    bias = bias.reshape(-1)
                    
                    X = self.conv_layer(X, kernels, bias)
                    
                elif layer_type == "relu":
                    X = tf.nn.relu(X)
                
                elif layer_type == "pool":
                    X = self.pool_layer(X, p_type)
                    
                CNN[layer_name] = X
                
        return CNN
    

