
# coding: utf-8

# In[ ]:


import numpy as np
from matplotlib import pyplot as plt
from PIL import Image

def image_loader(filename, shape=None):
    image = Image.open(filename)
    if shape is not None:
        image = image.resize(shape, Image.LANCZOS)
    
    img = np.asarray(image, dtype=np.float32)
    # VGG19 requires input dimension to be (batch, height, width, channel)
    img = np.expand_dims(img, axis=0)
    
    return img

def save_image(image, filename):
    image = np.clip(image, 0.0, 255.0).astype(np.uint8)
    with open(filename, "wb") as file:
        Image.fromarray(image).save(file,"jpeg")
        
        
def image_plotting(image_content, image_style, image_stylizer):
    fig, axis = plt.subplots(1, 3, figsize=(10, 10))
    
    fig.subplots_adjust(hspace=0.1, wspace=0.1)
    
    #plotting content image
    ploter = axis.flat[0]
    content_show = image_content / 255.0
    ploter.imshow(content_show, interpolation="sinc")
    ploter.set_xlabel("Content Image")
    
    #plotting style image
    ploter = axis.flat[1]
    style_show = image_style / 255.0
    ploter.imshow(style_show, interpolation="sinc")
    ploter.set_xlabel("Style Image")
    
    #plotting stylizer image
    ploter = axis.flat[2]
    stylizer_show = image_stylizer / 255.0
    ploter.imshow(stylizer_show, interpolation="sinc")
    ploter.set_xlabel("stylizer Image")
    
    
    for item in axis.flat:
        item.set_xticks([])
        item.set_yticks([])
        
    plt.show()

def image_single(image):
    img = np.reshape(image, image.shape[1:]).astype(np.uint8)
    
    plt.imshow(img)
    plt.show()
